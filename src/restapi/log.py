from typing import Tuple, List, Dict
from os import path
from time import time
from src.io import LOG_DIR

from src.restapi import files


def parse_log(line: str) -> Tuple[int, float]:
    try:
        values = line.strip().split(" ")
        return int(float(values[0])), float(values[1])
    except Exception:
        print("Error parsing line: %s" % line)
        return None


def get(offset: int = 0, count: int = 240, start: int = None, end: int = None) -> List[Dict]:
    if start is None:
        start = round(time())

    pings = []

    for filename in files.filenames():
        filename = path.join(LOG_DIR, filename)
        with open(filename, "r") as f:
            for line in reversed(f.readlines()):
                values = parse_log(line)
                if values is not None and values[0] <= start:
                    if offset > 0:
                        offset -= 1
                    elif count <= 0 or (end is not None and values[0] < end):
                        return pings
                    else:
                        pings.append({"time": values[0], "ping": values[1]})
                        count -= 1

    return pings
