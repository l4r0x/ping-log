from os import path, listdir
from flask import send_file, abort
from werkzeug.utils import secure_filename

from src.io import LOG_DIR

def filenames():
    files = listdir(LOG_DIR)
    files = sorted(files, reverse=True)
    return [secure_filename(f) for f in files]

def get(name):
    filename = path.join(LOG_DIR, secure_filename(name))
    if path.exists(filename):
        return send_file(filename)
    else:
        abort(404)
