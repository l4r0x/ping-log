import sys
import threading
import logging
from os import path, mkdir

from flask import send_file
import connexion
from argparse import ArgumentParser


parser = ArgumentParser()
parser.add_argument("target", help="Ping target host name")
parser.add_argument("-i", "--interval", help="Ping interval",
                    default=60, type=int)
parser.add_argument("-s", "--host", help="Server host name",
                    default="localhost", type=str)
parser.add_argument("-p", "--port", help="Server Port",
                    default=5000, type=int)
parser.add_argument("-d", "--debug", help="Run in debug mode",
                    action="store_true")
args = parser.parse_args()


APP_ROOT = path.abspath(path.dirname(__file__))
(APP_ROOT, _) = path.split(APP_ROOT)
sys.path.insert(0, APP_ROOT)

from src import ping
from src.io import (APP_ROOT, API_DIR, LOG_DIR, STATIC_DIR)


# create log directory
if not path.exists(LOG_DIR):
    mkdir(LOG_DIR)

options = {"swagger_ui": False} if not args.debug else None

connapp = connexion.FlaskApp(
    __name__, specification_dir=API_DIR, options=options)

app = connapp.app
app.config["APP_ROOT"] = APP_ROOT
app.config["LOG_DIR"] = LOG_DIR
app.static_folder = STATIC_DIR

connapp.add_api("api.yaml")

log = logging.getLogger("werkzeug")
log.setLevel(logging.WARNING)


@app.route("/")
def index():
    return send_file(path.join(APP_ROOT, "static", "index.html"))


ping_task = threading.Thread(target=ping.start_logger,
                             args=(args.target, LOG_DIR, args.interval),
                             daemon=True)
ping_task.start()

connapp.run(host=args.host, debug=args.debug, port=args.port)
