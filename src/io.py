from os import path

APP_ROOT = path.abspath(path.dirname(__file__))
(APP_ROOT, _) = path.split(APP_ROOT)

API_DIR = path.join(APP_ROOT, "src", "restapi")
LOG_DIR = path.join(APP_ROOT, "log")
STATIC_DIR = path.join(APP_ROOT, "static")
