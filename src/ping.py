#!/usr/bin/env python3

import subprocess
import time
from datetime import date, datetime
from os import path
from math import floor


def next_time(time: int, interval: int) -> int:
    return floor((time + interval) / interval) * interval


def parse_ping(result: str, verbose: bool = False) -> float:
    try:
        line = result.split("\n")[1]
        start_index = line.index("time=") + 5
        end_index = line.index(" ", start_index)
        ping = line[start_index:end_index]
        return float(ping)
    except Exception as e:
        if verbose:
            print(e)
        return 1000


def ping(host: str, verbose: bool = False) -> float:
    status, result = subprocess.getstatusoutput("ping -c1 -W1 " + host)

    if verbose:
        print(status)
        print(result + "\n")

    if status == 0:
        return parse_ping(result, verbose=verbose)
    else:
        return 1000


def start_logger(host: str, out: str = None, interval: int = 60,
                      verbose: bool = False):
    while (True):
        target = next_time(round(time.time()), interval)
        if verbose:
            print("wait until %d" % target)
        time.sleep(target - time.time())

        cur_ping = ping(host, verbose=verbose)

        if out:
            filename = date.today().strftime("%y%m%d") + ".txt"
            filename = path.join(out, filename)
            with open(filename, "a") as f:
                f.write("%d %f\n" % (target, cur_ping))
        else:
            print(target, cur_ping)


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("host", help="target host name")
    parser.add_argument("-i", "--interval", help="interval",
                        default=60, type=int)
    parser.add_argument("-v", "--verbose",
                        help="log output", action="store_true")
    parser.add_argument("-o", "--out", help="output directory")
    args = parser.parse_args()

    interval = max(1, min(args.interval, 3600))

    try:
        start_logger(args.host, args.out, interval, verbose=args.verbose)
    except KeyboardInterrupt:
        print("\nLoging stopped!")
