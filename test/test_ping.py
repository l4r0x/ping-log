from sys import path
path.insert(0, "src")
import ping


def test_next_time():
    assert 60 == ping.next_time(0, 60)
    assert 60 == ping.next_time(1, 60)
    assert 60 == ping.next_time(59, 60)
    assert 120 == ping.next_time(60, 60)
    assert 120 == ping.next_time(119, 60)
    assert 180 == ping.next_time(120, 60)


def test_parse_ping():
    ping.verbose = True

    result = """PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data."""
    assert 1000 == ping.parse_ping(result)

    result = """PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=120"""
    assert 1000 == ping.parse_ping(result)

    result = """PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=120 time=a ms"""
    assert 1000 == ping.parse_ping(result)

    result = """PING localhost (127.0.0.1) 56(84) bytes of data.
64 bytes from localhost (127.0.0.1): icmp_seq=1 ttl=64 time=0.051 ms"""
    assert 0.051 == ping.parse_ping(result)

    result = """PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=120 time=17.7 ms"""
    assert 17.7 == ping.parse_ping(result)

    result = """PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=120 time=15.7 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 15.724/15.724/15.724/0.000 ms"""
    assert 15.7 == ping.parse_ping(result)


def test_ping():
    ping.verbose = True
    assert 0 < ping.ping("localhost") < 1000
    assert 1000 == ping.ping("342342")
