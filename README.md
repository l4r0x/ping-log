# Ping Log

Simple RESTful webserver for logging and visualizing access times to a
specified host. It is designed for a raspberry pi or other unix based device
running permanently inside the network.

## Python Requirements

All other requirements are stored in the [requirements.txt](./requirements.txt) file.

### Development Requirements

* **unit-tests** - pytest
* **linting** - pylint, pep8
* **formatting** - autopep8

## Setup

The [virtualenv](https://virtualenv.pypa.io/en/latest/) module is highly recommended.

See https://virtualenv.pypa.io/en/stable/userguide/ for more information.


Setup all project dependencies:

```bash
# install requirements
pip install wheel
pip install -r requirements.txt
```

## Usage

### Start Server

```bash
python src/server.py 1.1.1.1
```

See `python src/server.py -h` for more information about the optional arguments.

> The log files are created per day and they store the unix timestamps and the
> corresponding access times for each ping request.
>
> RestAPI UI: http://{ip}[:{port}]/api/ui

The ping logger, which performs the ping requests, can also be executed directly.

```bash
python src/ping.py
```

## Tests

### Server Tests

Run the python unit tests.

```bash
# activate venv
source venv/bin/activate
# run tests
pytest
```

> Searches for python files prefixed with `test_` and executes all contained
> functions starting with `test_`.
